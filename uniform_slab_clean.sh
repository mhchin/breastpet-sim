#!/bin/bash
#SBATCH --job-name=test
#SBATCH --time=0:20:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=6G
#SBATCH --mail-type=ALL
#SBATCH --mail-user=dfreese@stanford.edu

# The sherlock clusters have different python software available, so include
# the appropriately named ones.
if [ $SHERLOCK -eq 1 ]; then
    module load python/2.7.5 py-numpy/1.10.4
elif [ $SHERLOCK -eq 2 ]; then
    module load py-scipystack/1.0_py27 python/2.7.13
fi
source $HOME/Gray/set_gray_dir.sh
srun hostname
srun python $HOME/breastpet-sim/uniform_slab_clean.py
