import numpy as np
import gray
import os

scratch = os.environ.get('SCRATCH')
paths = tuple(os.path.join(scratch, 'norm_sheet.coinc.dat.{}'.format(x)) for x in range(500))
data = np.concatenate(tuple(gray.load_variable_binary(p) for p in paths))
np.savez_compressed(os.path.join(scratch, 'norm_sheet.coinc.npz'), coinc=data)
print('coincidences: {0}'.format(data.size // 2))
