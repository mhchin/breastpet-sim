#!/bin/bash
#
#SBATCH --job-name=test
#
#SBATCH --time=1:00:00
#SBATCH --ntasks=1
#SBATCH --array=0-200
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=1G

srun hostname
srun $HOME/Gray/bin/gray -f $HOME/breastpet-sim/breast_panel.dff --seed ${SLURM_ARRAY_TASK_ID} --phys $HOME/breastpet-sim/GrayPhysics.json -c $SCRATCH/breast_panel.${SLURM_ARRAY_TASK_ID}.coinc.dat -t 15000
