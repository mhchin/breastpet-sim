#!/bin/bash
#SBATCH --job-name=test
#SBATCH --time=0:20:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=12G

module load python/2.7.5 py-numpy/1.10.4
source $HOME/Gray/set_gray_dir.sh
srun hostname
srun python $HOME/breastpet-sim/norm_sim_clean.py
