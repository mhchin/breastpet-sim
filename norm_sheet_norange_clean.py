import numpy as np
import gray
import os

name = 'norm_sheet_norange.coinc.dat'
output = name.replace('.dat', '.npz')
scratch = os.environ.get('SCRATCH')
paths = tuple(os.path.join(scratch, '{0}.{1}'.format(name, x)) for x in range(500))
data = np.concatenate(tuple(gray.load_variable_binary(p) for p in paths))
np.savez_compressed(os.path.join(scratch, output), coinc=data)
print('coincidences: {0}'.format(data.size // 2))
